﻿using Cli;
using FluentAssertions;
using NSubstitute;
using NSubstitute.ExceptionExtensions;

namespace UnitTests;

public sealed class RegisterUserUseCaseTests
{
    private readonly RegisterUserUseCase _useCase;
    private readonly IUserRepository _userRepository;
    private readonly IEmailSender _emailSender;

    public RegisterUserUseCaseTests()
    {
        _userRepository = Substitute.For<IUserRepository>();
        _emailSender = Substitute.For<IEmailSender>();
        _useCase = new RegisterUserUseCase(_userRepository, _emailSender);
    }
    
    [Fact]
    public async Task Should_Throw_If_User_Exists()
    {
        // Arrange
        _userRepository
            .GetAsync("example@gmail.com")
            .Returns(new User
            {
                Email = "example@gmail.com"
            });

        // Act
        var act = () => _useCase.HandleAsync("example@gmail.com");

        // Assert
        await act.Should().ThrowAsync<Exception>();
    }

    [Fact]
    public async Task Should_Save_In_Db_If_Not_Exists()
    {
        // Arrange
        _userRepository.GetAsync(Arg.Any<string>()).Returns(null as User);
        var email = "example@gmail.com";
        
        // Act
        await _useCase.HandleAsync(email);

        // Assert
        await _userRepository
            .Received(1)
            .SaveAsync(Arg.Is<User>(u => u.Email == email));
    }

    [Fact]
    public async Task Should_Throw_Exception_If_Saving_To_DB_Fails()
    {
        // Arrange
        _userRepository.GetAsync(Arg.Any<string>()).Returns(null as User);
        _userRepository.SaveAsync(Arg.Any<User>()).Throws(new InvalidOperationException());

        // Act
        var act = () => _useCase.HandleAsync("example@gmail.com");

        // Assert
        await act.Should().ThrowAsync<InvalidOperationException>();
    }

    [Fact]
    public async Task Should_Send_Registration_Email_If_Saved_In_Db()
    {
        // Arrange
        _userRepository.GetAsync(Arg.Any<string>()).Returns(null as User);

        // Act
        await _useCase.HandleAsync("example@gmail.com");

        // Assert
        await _emailSender
            .Received(1)
            .SendEmailAsync("example@gmail.com", Arg.Any<string>());
    }
}