﻿namespace Cli;

public interface IEmailSender
{
    Task SendEmailAsync(string email, string message);
}