﻿namespace Cli;

public interface IUserRepository
{
    Task<User?> GetAsync(string email);
    
    Task SaveAsync(User user);
}