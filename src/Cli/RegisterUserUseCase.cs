﻿namespace Cli;

public sealed class RegisterUserUseCase
{
    private readonly IUserRepository _userRepository;
    private readonly IEmailSender _emailSender;

    public RegisterUserUseCase(IUserRepository userRepository, IEmailSender emailSender)
    {
        _userRepository = userRepository;
        _emailSender = emailSender;
    }

    public async Task HandleAsync(string email)
    {
        var existingUser = await _userRepository.GetAsync(email);

        if (existingUser is not null)
        {
            throw new Exception("Already exists");
        }

        await _userRepository.SaveAsync(new User
        {
            Email = email
        });

        await _emailSender.SendEmailAsync(email, "You are registered");
    }
}